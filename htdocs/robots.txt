# See http://www.robotstxt.org/wc/robots.html
# for information about the file format.
# Contact gcc@gcc.gnu.org for questions.

User-agent: *
Disallow: /viewcvs
Disallow: /svn
Disallow: /cgi-bin/
Disallow: /bugzilla/buglist.cgi
Crawl-Delay: 60
