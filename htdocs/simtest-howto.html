<html>
  <head>
    <title>How to test GCC on a simulator</title>
  </head>

  <body>
    <h1>How to test GCC on a simulator</h1>

    <p>Several GCC targets can be tested using simulators.  These allow
    you to test GCC for a specific processor for which you don't have
    access to hardware, or for targets that have general characteristics
    you'd like to test, like endianness or word size.</p>

    <p>All of the instructions here start out from a directory we'll
    call <code>${TOP}</code>, which is initially empty.</p>

    <h2>Set up sources</h2>

    <p>Testing with a simulator requires use of a combined tree;
    you can't easily build newlib, required for simulator testing,
    outside of a combined tree, and the build of the other components
    is easiest in a combined tree.</p>

    <p>The combined tree contains GCC sources plus several modules of
    the <code>src</code> tree: <code>binutils</code> and
    <code>newlib</code> for the build and <code>sim</code> for the
    simulators. If you already build with a combined tree you can use
    your current setup; if not, these instructions will get you the
    sources you need.</p>

    <h3>Check out initial CVS trees</h3>

    <p>If you don't yet have either tree you'll need to do an initial
    check-outs.</p>

    <p>Check out mainline GCC:</p>

<pre>
cd ${TOP}
svn checkout svn://gcc.gnu.org/svn/gcc/trunk gcc
# This makes sure that file timestamps are in order initially.
cd ${TOP}/gcc
contrib/gcc_update --touch
</pre>

    <p>Check out the <code>src</code> tree:</p>

<pre>
cd ${TOP}
cvs -d :pserver:anoncvs@sourceware.org:/cvs/src login
# You will be prompted for a password; reply with "anoncvs".
cvs -d :pserver:anoncvs@sourceware.org:/cvs/src co binutils newlib sim
</pre>

    <h3>Update CVS trees</h3>

    <p>You can update existing CVS trees rather than starting from
    scratch each time.  Update the GCC tree using the
    <code>gcc_update</code> script, which touches generated files and
    handles directory changes in the tree.  Be sure to do this from
    the directory in which you did the original check-out, NOT in the
    combined tree:</p>

<pre>
cd ${TOP}/gcc
contrib/gcc_update
</pre>

    <p>Update the <code>src</code> tree with the same sequence of
    commands that you used to check out that tree initially, invoked from
    the <code>src</code> directory (NOT from within the combined tree).</p>

    <h3>Create a combined tree</h3>

    <p>Create a tree that consists of all of the files from the GCC and
    binutils/sim/newlib source trees (including several simulators in
    <code>src/sim</code>), with the GCC files overriding the
    binutils/sim/newlib files when there's a conflict.  It's done this
    way because the GCC files are the master copy.  To save on disk
    space, these commands actually make a tree of hard links rather
    than duplicating all the files:</p>

<pre>
cd ${TOP}
rm -rf combined
mkdir combined
cd src &amp;&amp; find . -print | cpio -pdlm ../combined &amp;&amp; cd ..
cd gcc &amp;&amp; find . -print | cpio -pdlmu ../combined &amp;&amp; cd ..
</pre>

    <h2>Build it</h2>

    <p>Use a recent version of GCC as the build compiler, no earlier
    than 2.95.</p>

    <p>The target name suitable for the simulator is usually `*-elf' for a
    target `*'.  There are some exceptions, for instance on powerpc
    it's powerpc-eabi or powerpc-eabisim.  Here we build
    <code>arm-elf</code>.</p>

<pre>
cd ${TOP}
mkdir build install
cd build
../combined/configure \
    --target=arm-elf --prefix=${TOP}/install
make
</pre>

    <h2>Test it</h2>

    <p>The only trick here is that you want DejaGNU to use the simulator
    rather than trying to run the output on the build system.  For
    example:</p>

<pre>
cd ${TOP}/build
make check-gcc check-target-libstdc++-v3 RUNTESTFLAGS=--target_board=arm-sim
</pre>
or just
<pre>
cd ${TOP}/build
make check RUNTESTFLAGS=--target_board=arm-sim
</pre>
to exercise the just-built gcc on every test-suite in the tree.

    <p>The only reliable way (apart from guessing that it's probably
    `*-sim') to find out the name of the target board is to look in the
    DejaGNU sources, in <code>/usr/share/dejagnu/baseboards</code>, for
    something that looks right.  Or you can use this table of combinations
    that at one time compiled, usable as test-targets with the
    instructions above.</p>

    <p>You can compare your test results against the archived results
    linked below to detect major problems.  As always, if you're testing
    a patch you should compare your results with and without the patch.</p>

    <p>The target characteristic can help you determine which targets to
    use to broaden the scope of your testing.  A cc0 target uses a
    single condition code register, cc0, which is set as a side effect
    of most instructions.  The compiler cannot insert other instructions
    between the instruction that sets cc0 and the one that uses it.
    Because of this, a cc0 target causes
    significantly different code to be generated and can matter more
    than word size and endianness in terms of detecting new breakage.</p>

<table border="0" cellspacing="5" cellpadding="5">
<tr valign="top" align="left">
 <th>Target</th><th>Simulator</th><th>Comments</th><th>Test Results</th>
</tr>

<tr valign="top">
 <td>arm-elf</td>
 <td>arm-sim</td>
 <td>32-bit word, little endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00592.html">3.3 20030509 (prerelease)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-04/msg00949.html">3.2.3 20030415 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>fr30-elf</td>
 <td>fr30-sim</td>
 <td>32-bit word, big endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00766.html">3.3 20030509 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>h8300-coff</td>
 <td>h8300-sim</td>
 <td>cc0 target, big endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00624.html">3.3 20030509 (prerelease)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-04/msg00967.html">3.2.3 20030415 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>m32r-elf</td>
 <td>m32r-sim</td>
 <td>32-bit word, big endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg01478.html">3.3</a>
 </td>
</tr>

<tr valign="top">
 <td>mips-elf</td>
 <td>mips-sim</td>
 <td>big endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2005-11/msg00636.html">4.1.0 20051112 (experimental)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00619.html">3.3 20030509 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>mipsisa64-elf</td>
 <td>mips-sim-idt64</td>
 <td>&nbsp;</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-06/msg00335.html">3.4 20030605 (experimental)</a>
 </td>
</tr>

<tr valign="top">
 <td>mn10300-elf</td>
 <td>mn10300-sim</td>
 <td>cc0 target, little endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00632.html">3.3 20030509 (prerelease)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-04/msg00972.html">3.2.3 20030415 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>powerpc-eabisim</td>
 <td>powerpc-sim</td>
 <td>32-bit word, big endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-03/msg00033.html">3.4 20030301 (experimental)</a>
 </td>
</tr>

<tr valign="top">
 <td>sh-coff</td>
 <td>sh-hms</td>
 <td>&nbsp;</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00622.html">3.3 20030509 (prerelease)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-04/msg00964.html">3.2.3 20030415 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>sh-elf</td>
 <td>sh-sim</td>
 <td>big endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2006-02/msg01441.html">4.2.0 20060226 (experimental)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-06/msg00315.html">3.4 20030605 (experimental)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00631.html">3.3 20030509 (prerelease)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-04/msg00970.html">3.2.3 20030415 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>sparc-elf</td>
 <td>sparc-sim</td>
 <td>&nbsp;</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-05/msg00587.html">3.3 20030507 (prerelease)</a>
 <br />
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-04/msg00942.html">3.2.3 20030415 (prerelease)</a>
 </td>
</tr>

<tr valign="top">
 <td>v850-elf</td>
 <td>v850-sim</td>
 <td>cc0 target, 32-bit word, little endian</td>
 <td>
 <a href="http://gcc.gnu.org/ml/gcc-testresults/2003-04/msg00978.html">3.2.3 20030415 (prerelease)</a>
 </td>
</tr>

</table>
</body>
</html>
